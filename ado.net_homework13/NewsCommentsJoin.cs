﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework13
{
    public class NewsCommentsJoin
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public int NewsId { get; set; }
        public string CommentBody { get; set; }

    }
}
