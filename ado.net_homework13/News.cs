﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbUp;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;


namespace ado.net_homework13
{
    public class News
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
    }
}
