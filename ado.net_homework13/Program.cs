﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using DbUp;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ado.net_homework13
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionStringConfiguration = ConfigurationManager.ConnectionStrings["appConnection"];
            var connectionString = connectionStringConfiguration.ConnectionString;
            //var providerName = connectionStringConfiguration.ProviderName;
            NewsServices newsServices = new NewsServices();
            #region migration
            EnsureDatabase.For.SqlDatabase(connectionString);

            var upgrader =
            DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();
            if (!result.Successful) throw new Exception("Migration not successful");

            #endregion

            while (true)
            {
                Console.Clear();
                string menu = "";
                Console.WriteLine("Press 1 to add News");
                Console.WriteLine("Press 2 to add Comment");
                Console.WriteLine("Press 3 to view all news");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Enter header text");
                        string header = Console.ReadLine();
                        Console.WriteLine("Enter news body");
                        string body = Console.ReadLine();
                        newsServices.Insert(connectionString,@"INSERT INTO News(Header, Body) VALUES(@Header, @Body)", new News
                        { 
                            Header = header,
                            Body = body
                        });
                        break;
                    case "2":
                        Console.WriteLine("Enter news Id");
                        string newsId = Console.ReadLine();
                        Console.WriteLine("Enter comment body");
                        string commentBody = Console.ReadLine();
                        if (Regex.IsMatch(newsId, @"^[0 - 9] +$") || newsId == "")
                        {
                            Console.WriteLine("news id must be digit");
                        }

                        else
                        {
                            newsServices.Insert(connectionString, @"INSERT INTO Comments(NewsId, CommentBody) VALUES(@NewsId, @CommentBody)", new Comment
                            {
                                CommentBody = commentBody,
                                NewsId = int.Parse(newsId)
                            });
                        }
                        break;
                    case "3":
                        var newsList = newsServices.GetAllNews<NewsCommentsJoin>(connectionString, @"select News.Id, News.Header, News.Body, Comments.CommentBody from News 
                        join Comments on News.Id=NewsId");
                        foreach (var item in newsList)
                        {
                            Console.WriteLine(item.Header);
                            Console.WriteLine(item.Body);
                            Console.WriteLine(item.CommentBody);
                        }
                        Console.ReadLine();
                        break;
                    default:
                        break;
                }
            }

            Console.ReadLine();

        }
    }
}
