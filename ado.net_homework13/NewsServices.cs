﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework13
{
   public class NewsServices
    {
        public T Insert<T> (string conString,string query, T myObject)
        {
            using (var sql = new SqlConnection(conString))
            {
                var result = sql.Execute(query, myObject);
                if (result < 1) throw new Exception("Error while Insert");
            }
            Console.WriteLine("Insert Done Successfully");
            return myObject;

        }

        public List<T> GetAllNews<T>(string conString, string query)
        {
            using (var sql = new SqlConnection(conString))
            {
                return sql.Query<T>(query).ToList();
            }

        }
    }
}
