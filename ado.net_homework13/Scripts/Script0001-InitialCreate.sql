﻿create table [dbo].[News]
(
Id int primary key IDENTITY(1,1),
Header nvarchar(50),
Body nvarchar(max)
)

create table [dbo].[Comments]
(
Id int primary key IDENTITY(1,1),
NewsId int,
CommentBody nvarchar(max)
)